export const selectorFavorite = (store) => store.favorite 
export const selectorBasket = (store) => store.basket 
export const selectorAllProducts = (store) => store.products 
export const selectorUserForm = (store) => store.userForm