import { createReducer } from "@reduxjs/toolkit"
import * as actions from "./actions"

const safeParseJSON = (item) => {
    try {
      return JSON.parse(item);
    } catch (e) {
      console.error(`Error parsing JSON from localStorage for item: ${item}`, e);
      return null;
    }
  };

export default createReducer(
    {
        favorite: safeParseJSON(localStorage.getItem("favorite")) || [],
        basket: safeParseJSON(localStorage.getItem("basket")) || [],
        products: [],
        userForm: {
            name: "",
            lastname: "",
            age: "",
            address: "",
            phone: "",
        }
    },
    (builder) => {
        builder
            .addCase(actions.actionAddFavorite, (state, { payload }) => {
                const findIndex = state.favorite.findIndex((item) => item.article === payload.article);
                if (findIndex === -1) {
                    state.favorite.push({ ...payload, isFavorite: true });
                    localStorage.setItem("favorite", JSON.stringify(state.favorite))
                }
                state.products = state.products.map((item) => {
                    if (item.article === payload.article) {
                        return { ...item, isFavorite: true }
                    }
                    return item;
                })
                localStorage.setItem('products', JSON.stringify(state.products))
            })

            .addCase(actions.actionRemoveFavorite, (state, { payload }) => {
                state.products = state.products.map((item) => {
                    if (item.article === payload.article) {
                        return {...item, isFavorite: false}
                    } 
                    return item;
                })
                localStorage.setItem('products', JSON.stringify(state.products))
                const newFavorite = state.favorite.filter((item) => item.article !== payload.article);
                state.favorite = newFavorite
                localStorage.setItem("favorite", JSON.stringify(state.favorite))
            })

            .addCase(actions.actionAddBasket, (state, { payload }) => {
                const findIndex = state.basket.findIndex((item) => item.article === payload.article);
                if (findIndex === -1) {
                    state.basket.push({ ...payload, isBasket: true, basketCounter: 1 })
                    localStorage.setItem("basket", JSON.stringify(state.basket))
                }
                if (findIndex >= 0) {
                    state.basket = state.basket.map((item, index) => {
                        if (index === findIndex) {
                            return {...item, basketCounter: item.basketCounter + 1}
                        } 
                        return item
                    })
                    localStorage.setItem("basket", JSON.stringify(state.basket))
                }
                state.products = state.products.map((item) => {
                    if (item.article === payload.article) {
                        return { ...item, isBasket: true, basketCounter: item.basketCounter + 1 }
                    }
                    return item;
                })
                localStorage.setItem('products', JSON.stringify(state.products))
            })

            .addCase(actions.actionRemoveBasket, (state, { payload }) => {
                state.products = state.products.map((item) => {
                    if (item.article === payload.article) {
                        return {...item, isBasket: false, basketCounter: 0}
                    } 
                    return item;
                })
                localStorage.setItem('products', JSON.stringify(state.products))
                const newBasket = state.basket.filter((item) => item.article !== payload.article)
                state.basket = newBasket
                localStorage.setItem("basket", JSON.stringify(state.basket))
            })

            .addCase(actions.actionFetchData.fulfilled, (state, { payload }) => {
                state.products = payload
            })

            .addCase(actions.actionIncreaseBasket, (state, {payload}) => {
                state.basket = state.basket.map((item) => {
                   if (item.article === payload.article) {
                       return {...item, basketCounter: item.basketCounter + 1}
                   } 
                   return item
                });
                localStorage.setItem("basket", JSON.stringify(state.basket))
            })
            .addCase(actions.actionDecreaseBasket, (state, {payload}) => {
                state.basket = state.basket.map((item) => {
                   if (item.article === payload.article && item.basketCounter > 0) {
                       return {...item, basketCounter: item.basketCounter - 1}
                   } 
                   return item
                });
                localStorage.setItem("basket", JSON.stringify(state.basket))
            })
            .addCase(actions.actionSendBuyInfo, (state, {payload}) => {
                const date = new Date()
                const basketInfoBuy = state.basket.map(({name, basketCounter, price}) => {
                    return {name, basketCounter, price, totalPrice: basketCounter*price}
                })
                state.userForm = {...payload, buyId: date.getTime(), products: [...basketInfoBuy]}
                console.log(state.userForm);
                state.products = state.products.map((item) => {
                    return {...item, isBasket: false}
                })
                localStorage.setItem('products', JSON.stringify(state.products))
                state.basket = []
                localStorage.removeItem("basket")
            })
    }
)

