import { Routes, Route } from "react-router-dom"
import NotPage from "../pages/NotPage/NotPage"
import FavoritePage from "../pages/FavoritePage/FavoritePage.jsx"
import CartPage from "../pages/CartPage/CartPage.jsx"
import AllProductPage from "../pages/AllProductPage/AllProductPage.jsx"
import OrderPage from "../pages/OrderPage/OrderPage.jsx"
import ThanksOrderPage from "../pages/ThanksOrderPage/ThanksOrderPage.jsx"

const AppRoutes = () => {

    return(
        <Routes>
            <Route path="/" element={<AllProductPage/>}/>
            <Route path="*" element={<NotPage/>}/>
            <Route path="/favorite" element={<FavoritePage/>}/>
            <Route path="/basket" element={<CartPage/>}/>
            <Route path="/order-list" element={<OrderPage/>}/>
            <Route path="/thanks-page" element={<ThanksOrderPage/>}/>
        </Routes>
    )
}

export default AppRoutes;