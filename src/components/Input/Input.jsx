import { Field, ErrorMessage } from "formik"
import "./Input.scss"
import PropTypes from "prop-types"
import cn from "classnames"

const Input = (props) => {
    const {
        label,
        type,
        placeholder,
        className,
        name,
        error,
        ...restProps
    } = props
    return (
        <label className={cn("input-block", className, { "error": error })}>
            <p className="input-block__title">{label}</p>
            <Field
                type={type}
                className="input"
                name={name}
                placeholder={placeholder}
                {...restProps}
            />
            <ErrorMessage name={name} className="error-message" component={"p"}/>
        </label>
    )
}

Input.defaultProps = {
    type: "text"
}

Input.propTypes = {
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    error: PropTypes.bool,
    restProps: PropTypes.any
}


export default Input;