import React from "react";
import PropTypes from "prop-types"
import Arrow from "../assets/icons/arrow.svg?react"
import Basket from "../assets/icons/basket1.svg?react"
import Like from "../assets/icons/like.svg?react"
import cn from "classnames"
import { useDispatch, useSelector } from "react-redux"

const Product = ({ data, onClick, onText, onImg }) => {

    const { image,
        name,
        price,
        article,
        color,
        isFavorite,
        isBasket
    } = data
    return (
        <div onClick={onClick} className="product">
            <img className="product__image" src={`images/${image}`} alt={name} />
            <Like onClick={onText} className={cn("like", {"like-red": isFavorite, "like-no": !isFavorite})} />
            <Basket onClick={onImg} className={cn("basket", {"basket-red": isBasket, "basket-no": !isBasket})} />
            <div className="product__info-box">
                <div className="product__text">
                    <h2 className="product__title">{name}</h2>
                    <p>Article: {article}</p>
                    <p>Price: {price} грн</p>
                    <p>Color: {color}</p>
                </div>
                <Arrow />
            </div>
        </div>
    )
}

Product.propTypes = {
    data: PropTypes.object,
    onClick: PropTypes.func,
    onText: PropTypes.func,
    onImg: PropTypes.func
}

export default Product