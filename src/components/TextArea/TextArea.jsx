import { Field, ErrorMessage } from "formik"
import "./TextArea.scss"
import PropTypes from "prop-types"
import cn from "classnames"

const TextArea = (props) => {
    const {
        label,
        type,
        placeholder,
        className,
        name,
        error,
        rows,
        ...restProps
    } = props
    return (
        <label className={cn("input-block", className, { "error": error })}>
            <p className="input-block__title">{label}</p>
            <Field
                as={"textarea"}
                rows={rows}
                type={type}
                className="input"
                name={name}
                placeholder={placeholder}
                {...restProps}
            />
            <ErrorMessage name={name} className="error-message" component={"p"} />
        </label>
    )
}

TextArea.defaultProps = {
    type: "text"
}

TextArea.propTypes = {
    rows: PropTypes.number,
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    error: PropTypes.bool,
    restProps: PropTypes.any
}


export default TextArea;