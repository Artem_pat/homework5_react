import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import "./Button.scss"

const Button = (props) => {
    const { type,
        onClick,
        className,
        children,
        ...restProps
    } = props

    return (
        <button
            onClick={onClick}
            className={cn("btn", className)}
            type={type}
            {...restProps}
        >
            {children}
        </button>
    )
}

Button.defaultProps = {
    type: "button"
}

Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.any,
    restProps: PropTypes.object,
}

export default Button;