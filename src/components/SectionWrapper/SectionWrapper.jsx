import React from "react";
import PropTypes from "prop-types";
import "./SectionWrapper.scss"

const SectionWrapper = ({children}) => {
    return(
        <section onClick={onclick} className="container">
            {children}
        </section>
    )
}

SectionWrapper.propTypes = {
    children: PropTypes.any,
}

export default SectionWrapper