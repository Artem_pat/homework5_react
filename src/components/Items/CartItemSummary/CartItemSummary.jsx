import "./CartItemSummary.scss"

const CartItemSummary = ({ data }) => {
    const { image,
        name,
        price,
        article,
        color,
        basketCounter,
    } = data
    // const totalPrice = price*basketCounter;
    return (
        <div className="item-box cart-box">
            <img className="item-img" src={`images/${image}`} alt={name} />
            <div className="item-info">
                <h2 className="item-info__title">{name}<span className="item-info__count"> х {basketCounter}</span></h2>
                <p className="item-info__text item-info__text--md">article: {article}</p>
                <p className="item-info__text">color: {color}</p>
            </div>
            <p className="item-price">{price*basketCounter} грн</p>
        </div>
    )
}

export default CartItemSummary;