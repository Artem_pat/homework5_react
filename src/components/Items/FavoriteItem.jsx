import React from "react";
import CrossIcon from "../../assets/icons/cross.svg?react"
import "./Items.scss"
import Button from "../Button/Button";
import { useDispatch } from "react-redux"
import { actionRemoveFavorite } from "../../store/actions"

const FavoriteItem = ({data, onDel}) => {

    const dispatch = useDispatch()

    const handleRemoveFavorite = (item) => {
        dispatch(actionRemoveFavorite(item))
    }
    const { image,
        name,
        price,
        article,
        color,
        fill } = data
    return(
        <div className="item-box favorite-box">
            <CrossIcon className="item-del" onClick={() => handleRemoveFavorite(data)}/>
            <img className="item-img" src={`images/${image}`} alt={name} />
            <div className="item-info">
                <h2 className="item-info__title">{name}</h2>
                <p className="item-info__text item-info__text--md">article: {article}</p>
                <p className="item-info__text">color: {color}</p>
            </div>
            <Button className={"btn-secondary"} onClick={onDel} >Add To Cart</Button>
            <p className="item-price">{price} грн</p>
        </div>
    )
}

export default FavoriteItem;