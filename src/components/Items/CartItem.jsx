import React from "react";
import DelIcon from "../../assets/icons/deleteicon.svg?react"
import "./Items.scss"
import Button from "../Button/Button";

const CartItem = ({ data, onCloseModal, onAddBasket, onCartCountPlus, onCartCountMinus }) => {

    const { image,
        name,
        price,
        article,
        color,
        basketCounter } = data
    return (
        <div className="item-box cart-box">
            <img className="item-img" src={`images/${image}`} alt={name} />
            <div className="item-info">
                <h2 className="item-info__title">{name}</h2>
                <p className="item-info__text item-info__text--md">article: {article}</p>
                <p className="item-info__text">color: {color}</p>
            </div>
            <Button className={"btn-secondary"} onClick={() => {
                onAddBasket()
            }}>Add To Favorite</Button>
            <div className="cart-box__number-box">
                <span onClick={onCartCountMinus} className="cart-box__number-minus">-</span>
                <span className="item-number">{basketCounter}</span>
                <span onClick={onCartCountPlus} className="cart-box__number-plus">+</span>
            </div>
            <p className="item-price">{price} грн</p>
            <DelIcon className="item-del" onClick={onCloseModal} />
        </div>
    )
}

export default CartItem;