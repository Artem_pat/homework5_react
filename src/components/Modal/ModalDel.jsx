import React from "react";
import ModalWrapper from "./ModalWrapper.jsx";
import ModalBody from "./ModalBody.jsx";
import ModalClose from "./ModalClouse.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalFooter from "./ModalFooter.jsx";
import "./Modal.scss"
import ModalContent from "./ModalContent.jsx";

const ModalDel = ({ close, name, color, price, image, del }) => {

    return (
        <ModalWrapper onClick={close}>
            <ModalBody className="modal">
                <ModalClose onClick={close} />
                <img className="img" src={`images/${image}`} alt={name} />
                <ModalHeader>
                    Видалити: {name}
                </ModalHeader>
                <ModalContent>
                    <p>колір: {color}</p>
                    <p>ціна: {price}грн</p>
                </ModalContent>
                <ModalFooter firstClick={close} firstText="no" secondaryClick={() => {
                    close()
                    del()
                }} secondaryText="yes" />
            </ModalBody>
        </ModalWrapper>
    )

}

export default ModalDel;