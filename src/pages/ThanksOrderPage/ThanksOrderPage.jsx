import { Link } from "react-router-dom"
import "./ThanksOrderPage.scss"
import { useSelector } from 'react-redux'

const ThanksOrderPage = () => {
    const buyId = useSelector((state) => state.userForm).buyId;
    console.log(buyId);
    return(
        <div>
            <h1 className="order-title">Thank you for your order: #{buyId}!!! You can go to <Link className="to-main-page" to={"/"}>main page</Link></h1>

        </div>
    )
}

export default ThanksOrderPage;