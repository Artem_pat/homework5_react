import { useSelector, useDispatch } from "react-redux"
import { selectorBasket, selectorUserForm } from "../../store/selectors"
import { actionSendBuyInfo } from "../../store/actions"
import CartItemSummary from "../../components/Items/CartItemSummary/CartItemSummary";
import "./OrderPage.scss"
import { Form, Formik } from "formik"
import { validation } from "./validation"
import Input from "../../components/Input/Input";
import Button from "../../components/Button/Button";
import TextArea from "../../components/TextArea/TextArea";
import { useNavigate } from "react-router-dom";
import { PatternFormat } from "react-number-format";

const OrderPage = () => {
    const navigate = useNavigate();
    const basketProducts = useSelector(selectorBasket);
    const userForm = useSelector(selectorUserForm);
    const dispatch = useDispatch()
    const totalPrice = basketProducts.reduce((accum, item) => {
        accum += (item.price * item.basketCounter)
        return accum
    }, 0)

    return (
        <div className="order-list-wrapper">
            <div className="billing-details">
                <h3 className="billing-details__title">Billing Details</h3>
                <div className="form-box">
                    <Formik
                        initialValues={userForm}
                        validationSchema={validation}
                        onSubmit={(values, { resetForm }) => {
                            dispatch(actionSendBuyInfo(values))
                            // console.log(userForm)
                            resetForm()
                            navigate('/thanks-page');
                        }}
                    >
                        {({ errors, touched, setFieldValue }) => (
                            <Form>
                                <fieldset>
                                    <div className="inputs-box">
                                        <div className="input-box-1">
                                            <Input
                                                className={""}
                                                type="text"
                                                label="Name"
                                                placeholder="Enter your name"
                                                name="name"
                                                error={errors.name && touched.name}
                                            />
                                            <Input
                                                className={""}
                                                type="text"
                                                label="Lastname"
                                                placeholder="Enter your lastname"
                                                name="lastname"
                                                error={errors.lastname && touched.lastname}
                                            />
                                        </div>
                                        <div className="input-box-2">
                                            <Input
                                                className={""}
                                                type="text"
                                                label="Age"
                                                placeholder="Age"
                                                name="age"
                                                error={errors.age && touched.age}
                                            />
                                            <PatternFormat
                                                customInput={Input}
                                                format="+38 (###) ### ## ##"
                                                mask="_"
                                                onValueChange={({ value }) => {
                                                    setFieldValue('phone', `+38${value}`);
                                                  }}
                                                className={""}
                                                type="text"
                                                label="Phone"
                                                placeholder="+38(xxx)-xx-xx"
                                                name="phone"
                                                error={errors.phone && touched.phone}
                                            />
                                        </div>
                                    </div>
                                    <div className="text-area-box">
                                        <TextArea
                                            type="text"
                                            label="Addres"
                                            placeholder="Addres"
                                            rows={3}
                                            name="address"
                                            error={errors.addres && touched.addres}
                                        />
                                    </div>
                                </fieldset>
                                <Button type="submit">Continue to delivery</Button>
                            </Form>
                        )}

                    </Formik>
                </div>
            </div>
            <div className="order-summary">
                <h3 className="order-summary__tittle">Order Summary</h3>
                {basketProducts.map((item) => {
                    return <CartItemSummary key={item.article} data={item} />
                })}
                <div className="total">
                    <p className="total__title">Total</p>
                    <p className="total__price">{totalPrice} грн</p>
                </div>
            </div>
        </div>
    )
}

export default OrderPage;