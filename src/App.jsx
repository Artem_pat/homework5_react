import React, { useEffect, useState } from 'react'
import AppRoutes from './routes/index.jsx'
import HeaderSection from './components/HeaderSection/HeaderSection.jsx'
import SectionWrapper from './components/SectionWrapper/SectionWrapper.jsx'

function App() {
  
  return (
    <>
      <SectionWrapper>
        <HeaderSection />
        <main className="product-wrapper">
          <AppRoutes />
        </main>
      </SectionWrapper>
    </>
  )
}

export default App
